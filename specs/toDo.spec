To Do List
=====================


Proje yapilacak ekle
---------------------
tags:proje_yapilacak_ekle

*Anasayfayi yukle
*"liste" itemi olmadıgının kontrolunu yap
*Listeye "Proje Yapılacak" ekle
*"0" siradaki "projeYapilacak" konumunu kontrol et

Bilgisayar al ekle
------------------
tags:bilgisayar_al_ekle

*Anasayfayi yukle
*"0" siradaki "projeYapilacak" konumunu kontrol et
*Listeye "Bilgisayar Al" ekle
*"bilgisayarAl" iteminin "1" "projeYapilacak" iteminin "0" sirada oldugunu kontrol et

Mark et ve kontrol et
---------------------
tags:mark_et_ve_kontrol_et

*Anasayfayi yukle
*"bilgisayarAl" iteminin "1" "projeYapilacak" iteminin "0" sirada oldugunu kontrol et
*"0" siradaki "radioButon" radio butonuna tikla
*"0" siradaki "projeYapilacakMarked" konumunu kontrol et

Mark kaldir ve kontrol et
-------------------------
tags:mark_kaldir_ve_kontrol_et

*Anasayfayi yukle
*"0" siradaki "projeYapilacakMarked" konumunu kontrol et
*"0" siradaki "radioButon" radio butonuna tikla
*"0" siradaki "projeYapilacak" konumunu kontrol et

Sil ve kontrol et
-----------------
tags:sil_ve_kontrol_et

*Anasayfayi yukle
*"bilgisayarAl" iteminin "1" "projeYapilacak" iteminin "0" sirada oldugunu kontrol et
*Listeye "Heyseyi duzenli yap" ekle
*"2" siradaki "herSeyiDuzenliYap" konumunu kontrol et
*"0" siradaki "projeYapilacak" sil
*"projeYapilacakSilindi" itemi olmadıgının kontrolunu yap